(ns csv.internal
  (:require [clojure.string :refer [includes?] :as s]))

(defn header-to-string
  "Converts header keyword `key` to a string with spaces for dashes an no leading colon"
  [key]
  (if (keyword? key) (-> key (name) (s/replace "-" " ")) key))

(defn surround-with-quotes
  "Concatenates quotation marks around `v`"
  [v]
  (str "\"" v "\""))

(defn append-newline
  "Concatenates a newline to the end of `v`"
  [v]
  (str v "\n"))

(defn create-row-from-col
  "Creates a row for a csv from a `collection`, inserting commas between values and a newline at the end"
  [collection]
  (->> collection (s/join ",") (append-newline)))

(defn csvify
  "Takes a string, `value`, and conforms it to the RFC 4180 specification"
  [value]
  (let [value (if (string? value) value (str value))]
    (if (or (includes? value ",") (includes? value "\"") (includes? value "\n"))
      (-> value
          (s/escape {\"  "\"\""})
          (surround-with-quotes))
      value)))

(defn create-headers
  "Creates a vector of headers from all the keys in every map in `col-of-maps`"
  [col-of-maps order-by]
  (order-by (into [] (reduce #(into %1 (keys %2)) #{} col-of-maps))))

(defn prepare-values-of-map
  "Takes a map, `item`, and prepares its values to be in a csv string while also filling in all missing fields according to the headers in the collection ,`headers`"
  [headers val-fn-map item]
  (for [key headers]
    (let [val-fn (get val-fn-map key identity)
          value (get item key)]
      (if value (csvify (val-fn value)) ""))))
