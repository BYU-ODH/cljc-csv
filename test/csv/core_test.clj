(ns csv.core-test
  (:require [clojure.test :refer :all]
            [csv.core :as subj]
            [csv.internal :as private]
            [clojure.string :refer [capitalize]]))

(deftest write-csv
  (testing "Properly writes a csv file"
    (let [data [{:a "a" :b "b," :c "\"jimbo,\""} {:a "timmy" :d "\"" :b ",,,"}]
          csv (subj/write-csv data)]
      (spit "test.csv" csv)
      (is csv)))
  (testing "Handles maps with non-keyword keys"
    (let [data [{"a" "a" "b" "c"} {"a" "c" "d" "b"}]
          csv (subj/write-csv data)]
      (spit "test2.csv" csv)
      (is csv)))
  (testing "`display-fn`, `order-fn`, and `val-fn-map` work properly"
    (let [data [{:a-b-c "a" :b "b," :superman-rocks "\"jimbo,\""} {:a-b-c "timmy" :d "\n\"" :b ",,,"}]
          sort-fn (fn [_] [:b :d :a-b-c :superman-rocks])
          csv (subj/write-csv data {:display-fn (comp capitalize private/header-to-string) :order-fn sort-fn :val-fn-map {:a-b-c (fn [_] "guy")}})]
      (spit "test3.csv" csv)
      (is csv))))
