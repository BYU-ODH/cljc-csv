(defproject edu.byu.odh/cljc-csv "2"
  :description "A CLJC library designed to take a collection of maps and produce a csv string according to the RFC 4180 specification."
  :url "https://gitlab.com/BYU-ODH/cljc-csv"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :repositories [["releases" {:url "https://repo.clojars.org"
                              :creds :gpg}]]
  :dependencies [[org.clojure/clojure "1.10.0"]]
  :repl-options {:init-ns csv.core})
