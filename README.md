# cljc-csv

A Clojure library designed to take a collection of maps and produce a csv string according to the [RFC 4180](https://datatracker.ietf.org/doc/html/rfc4180) specification.

## Purpose

Currently there is no cljc solution for writing to csv. This library provides that functionality.

## Usage

**Basic Usage**
```
(def easy-example [{:name "John" :salary "$50k"} {:name "Susan" :salary "$60k"}])

(write-csv easy-example)
```
which returns
```
"name,salary\nJohn,$50k\nSusan,$60k\n"
```

**Advanced Usage**
```
(def advanced-example [{:name "George Washington" :profession "president" :notes "Great guy!"}
{:name "Santa Claus" :profession "toy-maker" :height "5'7\"" :notes "makes really fun toys, also doesn't like \"cookies\"\nMaybe we should leave something else out for him"}
{:name "Trevor" :profession "doctor" :height "6'0\"" :notes "n/a"}])

(write-csv advanced-example {:display-fn (comp clojure.string/capitalize name) :order-fn sort :val-fn-map {:name #(clojure.string/replace % "a" "e") :profession clojure.string/capitalize}})
```
which returns

```
"Height,Name,Notes,Profession\n,George Weshington,Great guy!,President\n\"5'7\"\"\",Sente Cleus,\"makes really fun toys, also doesn't like \"\"cookies\"\"\nMaybe we should leave something else out for him\",Toy-maker\n\"6'0\"\"\",Trevor,n/a,Doctor\n"
```

## Functions
| Function         | Arguments                  | Description                                                                                                                                                                                                                                                                              |
|------------------|----------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| write-csv        | `col-of-maps` & `controls` | `col-of-maps` is the collection of maps written to a csv string. `controls` is a map that contains function under :display-fn, :order-fn keys and val-fn-map; the `display-fn` will be applied to all header strings and by default will transform all keywords into strings with spaces instead of dashes. `order-fn` will be given the vector of headers and returns them in the order desired and uses identity by default. `val-fn-map` is a map for modifying the values of a certain column. The key is the header and the value is a function that is given the values in that column and returns them how the values should be displayed. The headers in the csv will be generated from all the keys in all the maps in `col-of-maps` |
## Details
Values are separated by commas, if the value contains a comma, newline character, or quotation marks then the entire value is surronded with quotation marks and all quotation marks in that value are escaped as two quotation marks (" -> ""). 

`write-csv` first finds all the headers for your csv string which it takes from the keys of all the maps in your collection of maps that you give it. After all the column headers are found, it goes through every value in those maps and if one of them is missing a column field, it fills in an empty string.

`write-csv` takes a map as an optional argument that I like to refer to as the controls. 

The user can include a `display-fn` which will be applied to each header. For example, if all the keys in your map are keywords and you want them displayed as strings you could use
```{:display-fn name}```
as your controls map and all your headers would be transformed into strings without the leading colons.

The user can include a `order-fn` which will be given the vector of all the headers and returns a vector of the headers in the desired order. For example, if you desire your headers to be displayed in alphabetical order, you could use ```{:order-fn sort}``` as your controls map.

The user can include a `val-map-fn` which will be a map of header keys to functions. If you would like to modify every value in a column, this is the place to do it. If I wanted every value in the `:name` column to be capitalized, I would use ```{:val-fn-map {:name clojure.string/capitalize}}``` as my control map.

## Alternatives

[data.csv](https://github.com/clojure/data.csv) writes to csv for .clj and [clojurescript.csv](https://github.com/testdouble/clojurescript.csv) writes to csv for .cljs

These libraries also provide csv reading which we do not yet provide.

## License

Copyright © 2022

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
