(ns csv.core
  "The public functions for writing to csv"
  (:require [csv.internal :as private]))

(defn write-csv
  "`col-of-maps` is the collection of maps written to a csv string. `controls` is a map that contains function under :display-fn, :order-fn keys and val-fn-map; the `display-fn` will be applied to all header strings and by default will transform all keywords into strings with spaces instead of dashes. `order-fn` will be given the vector of headers and returns them in the order desired and uses identity by default. `val-fn-map` is a map for modifying the values of a certain column. The key is the header and the value is a function that is given the values in that column and returns them how the values should be displayed. The headers in the csv will be generated from all the keys in all the maps in `col-of-maps`"
  [col-of-maps & [{:as controls :keys [display-fn order-fn val-fn-map] :or {display-fn private/header-to-string order-fn identity}}]]
  (let [headers (private/create-headers col-of-maps order-fn)
        data (->> col-of-maps
                 (into [] (map (partial private/prepare-values-of-map headers val-fn-map)))
                 (into [] (map private/create-row-from-col))
                 (apply str))
        headers-str (->> headers
                        (into [] (comp (map display-fn) (map private/csvify)))
                       (private/create-row-from-col))]
    (str headers-str data)))
