(ns csv.internal-test
  (:require [clojure.string :as string]
            [clojure.test :refer :all]
            [csv.internal :as subj]))

(deftest csvify
  (testing "Properly escapes commas"
    (let [value "joe,mama"
          value (subj/csvify value)
          expected-value "\"joe,mama\""]
      (is (= expected-value value))))
  (testing "Properly escapes quotes when commas are present"
    (let [value "joe,ma\"ma"
          value (subj/csvify value)
          expected-value "\"joe,ma\"\"ma\""]
      (is (= expected-value value))))
  (testing "Properly escape quotes when there are no commas"
    (let [value "joema\"ma"
          value (subj/csvify value)
          expected-value "\"joema\"\"ma\""]
      (is (= expected-value value))))
  (testing "Properly escapes newline characters"
    (let [value "I am a jelly\ndonut\""
          value (subj/csvify value)
          expected-value "\"I am a jelly\ndonut\"\"\""]
      (is (= expected-value value)))))

(deftest header-to-string
  (testing "keywords transformed properly"
    (let [value :meep-meep
          value (subj/header-to-string value)
          expected-value "meep meep"]
      (is (= expected-value value))))
  (testing "strings stay as strings"
    (is (= "-" (subj/header-to-string "-")))))

(deftest create-headers
  (testing "Creates the correct headers"
    (let [data [{:a "a" :b "b" :c "c"} {:d "d"} {:a "a" :e "e"}]
          expected-headers #{:a :b :c :d :e}
          expected-order [:a :b :c :d :e]
          headers (subj/create-headers data sort)]
      (is (loop [e expected-headers a headers]
            (cond
              (and (empty? a) (empty? e)) true
              (e (first a)) (recur (disj e (first a)) (rest a))
              :else false)))
      (is (= expected-order headers)))))

(deftest prepare-values-of-map
  (testing "makes an item with the correct headers"
    (let [headers [:a :b :c]
          item {:a "a" :c "c"}
          item2 {:a "j" :b "k" :c "l"}
          result (subj/prepare-values-of-map headers {:a string/capitalize} item)
          expected [(subj/csvify "A") "" (subj/csvify "c")]
          result2 (subj/prepare-values-of-map headers {} item2)
          expected2 [(subj/csvify "j") (subj/csvify "k") (subj/csvify "l")]]
      (is (= expected result))
      (is (= expected2 result2)))))
